#!/usr/bin/env python
# coding: utf-8

# In[1]:


from package import NewBEMiscFunctions
import locale
import os
import shutil
import re
import pandas as pd
import tkinter as tk
from dateutil.relativedelta import relativedelta
from xlrd import open_workbook


# # Initialisation des variables

# In[2]:


#Configuration des dossiers racines
ImportPath = '//fs02mauk/NGSS/French Retail/France Back Office/10. DOWNLOADS/CSVs/Done/'
ImportLst = os.listdir(ImportPath)
LocalAFACDir = 'C:/Process_Mohamed/ServiceCharges/Services/'
NetworkDir = '//fs02mauk/NGSS/French Retail/France Back Office/Rajae/'


# In[3]:


#Configuration des variables et des entêtes
AfacPattern = '^AFAC_A_.*_(.*)_(.*)_.*_(.*)_(.*)\.CSV'

AfacColumns = ["Période de facturation", "ZET", "PDLA", "Champ libre fournisseur PDLA", "Tarif", "Fréquence de relève",
"Type de PDLA", "Période de facturation antérieure", "Ajustement manuel", "Annulation", "Date Initiale",
"Date fin / Date prestation", "Type", "Type de terme détaillé", "Désignation complémentaire",
"Quantité", "Prorate temporis appliqué", "Prix unitaire", "Montant HT", "Taux de TVA", "Référence externe demande SI Fournisseur",
"Coefficient de commune", "Nombre de logements", "Numéro de PCE", "N° de la demande interne",""]


# In[4]:


AfacOrder = ["PDLA",
              "Date Initiale",
              "Date fin / Date prestation",
              "Type",
              "Type de terme détaillé",
              "Montant HT",
              ]


# In[5]:


#On créé un dictionnaire pour renommer les colonnes par rapport aux noms techniques SF
ColumnsToRename = {
    'Billing Period' : 'Periode_de_facturation__c',
    'SiteCRMPlantID__c' : 'CRM_Plant_ID__c',
    'ID Type Ajustement' : 'Type__c',
    'Description' : 'Intitule__c',
    'Montant HT' : 'Adjustement_Montant__c',
   'Type de terme détaill' : 'Type de terme détaillé,',
}


# In[6]:


#On créé une liste pour trier les colonnes dans le bon ordre
ColumnOrder = [
     'CRM_Plant_ID__c',
     'Periode_de_facturation__c',
     'Type__c',
     'Intitule__c',
     'Adjustement_Montant__c',
     'Type de terme détaillé',
]


# In[7]:


#Extraction des PDL depuis l'objet Site Billing Data depuis Salesforce
SoqlSBD = "SELECT SiteCRMPlantID__c, dPDL__c, DataValidFrom__c  "
SoqlSBD += " FROM SiteBillingData__c "
SoqlSBD += " Where IsSBDOutputFieldsComputed__c = True and IsObsolete__c = false And DataValidFrom__c < TODAY "


# # Import

# In[8]:


#Classe pour ouvrir une popup et demander à l'utilisateur de saisir le mois pour lequel il veut générer le fichier AFAC
class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        master.title('Split Meter Volume')
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.l1 = tk.Label(root, text='Veuillez spécifier le mois de consommation des sous-compteurs (format = yyyyMM)')
        self.l1.pack(side='top', padx=100, pady=5)
        
        self.e1 = tk.Entry(root)
        self.e1.pack(pady=5)
        self.e1.focus_set()    
    
        
        self.b = tk.Button(root,text='OK',command=self.getText)
        self.b.pack(side='bottom', pady=5)

    def getText(self):
        global ret1
        ret1 = self.e1.get().strip()
        root.destroy()
        
#On ouvre une popup pour définir le mois pour lequel on va extraire les volumes Split Meter
root = tk.Tk()
app = Application(master=root)
app.mainloop()


# In[9]:


# Les variables renseignées manuellement sont mappées sur des variables
DeliveryMonth = ret1
#Constante du mois de facturation
FirstBillingDayOfMonth = (pd.to_datetime(DeliveryMonth, format='%Y%m')) + relativedelta(months=+1)


# In[10]:


#On extrait la data depuis le réseau
Dir = LocalAFACDir
if os.path.exists(Dir):
    shutil.rmtree(Dir)
os.makedirs(Dir)
#Pour tout fichier sur le réseau, on extrait chaque fichier qui a le format "AFAC_A" où la date du fichier 
#est la date remplie lors de la popup
for file in ImportLst:
    m = re.match(AfacPattern, file.upper())
    if m:
        [Version, Grd, Time, Ref] = m.groups()
        if(Time[: 6] == DeliveryMonth):
            print(file)
            shutil.copy2(ImportPath + file, LocalAFACDir)


# In[11]:


#Tris des dossier par le plus récent au plus ancien afin de ne lire que les versions les plus récentes
localAgloLst = os.listdir(LocalAFACDir)
localAgloLst.sort(reverse=True)


# In[12]:


#Suppression des fichiers les plus anciens, concaténation des fichiers et lecture du fichier concaténné
AFACDf = pd.DataFrame(columns=range(len(AfacColumns)))
listId = []

for file in localAgloLst:
    m = re.match(AfacPattern, file.upper())
    [Version, Grd, Time, Ref] = m.groups()
    Id = Grd
    if(Id not in listId):
        listId = listId + [Id]
        print(Id, file)
        currentDf = pd.read_csv(LocalAFACDir + file, skiprows=2, skipfooter=1, sep=';', header=None, dtype=str, engine = 'python')
        AFACDf = pd.concat([AFACDf, currentDf])
        print(AFACDf.shape)

AFACDf.columns = AfacColumns
AFACDf['Date Initiale'] = pd.to_datetime(AFACDf['Date Initiale'])
AFACDf['Date fin / Date prestation'] = pd.to_datetime(AFACDf['Date fin / Date prestation'])
AFACDf = AFACDf.reset_index()


# # TRAITEMENT

# In[13]:


# On extrait la data depuis Salesforce
SalesforceExtractDF = NewBEMiscFunctions.SalesforceExtract(SoqlSBD)


# In[14]:


#On ne garde que les Site Billing Data inférieur à la date de facturation spécifiée
SalesforceExtractDF['DataValidFrom__c'] = pd.to_datetime(SalesforceExtractDF['DataValidFrom__c'])
SalesforceExtractDF = SalesforceExtractDF.loc[(SalesforceExtractDF['DataValidFrom__c'] < FirstBillingDayOfMonth)]


# In[15]:


#On ne garde que le PDL du Site Billing Data le plus récent
SalesforceExtractDF = SalesforceExtractDF.sort_values(by='DataValidFrom__c', ascending=False).drop_duplicates(subset=['dPDL__c'])


# In[16]:


#On ne garde que les types de terme 0081, 9080, 9081, 0080, 80 et 81 comme renseigné dans le fichier sur le GTG comme étant les prestations GRDF à facturer 
AFACDf = AFACDf.loc[(AFACDf['Type'] == '0081') | (AFACDf['Type'] == '9080') | (AFACDf['Type'] == '9081') | (AFACDf['Type'] == '0080') | (AFACDf['Type'] == '80') | (AFACDf['Type'] == '81')]


# In[17]:


#On remet les colonnes dans le bon ordre
AFACDf = AFACDf[AfacOrder]


# In[18]:


#Conversion des montants en float, la conversion en .astype(float) ne permet pas de corriger un Integer avec le - après le nombre
AFACDf['Montant HT'] = AFACDf['Montant HT'].apply(lambda x : x if x.replace('.','',1).isdigit() else str(float(x[:-1]) * -1))


# In[19]:


book = open_workbook('W:\French Retail\France Back Office\Rajae\Annexes 1 et 2 - GI AFAC.xlsx')
sheet = book.sheet_by_name('Annexe 2')

mapping = {}
for row_index in range(1,sheet.nrows):
    cell_value_class = sheet.cell(row_index, 0).value
    cell_value_class = str(int(cell_value_class)) if type(cell_value_class) == float else cell_value_class

    cell_value_id = sheet.cell(row_index, 1).value
    mapping[cell_value_class] = cell_value_id
    


# In[20]:


#Application du mapping du terme détaillé en description


AFACDf['Description'] = AFACDf['Type de terme détaillé'].map(mapping)

#Gestion des dates littérales pour chaque élément du dataframe
locale.setlocale(locale.LC_TIME,'')
AFACDf.loc[(AFACDf['Date Initiale'] != AFACDf['Date fin / Date prestation']), ['Description']] = AFACDf['Description'] + " - " +(AFACDf['Date Initiale'].apply(lambda x: x.strftime('%d %B %Y')))+" - "+" "+(AFACDf['Date fin / Date prestation'].apply(lambda x: x.strftime('%d %B %Y')))+" ("+AFACDf['Type de terme détaillé']+")"
AFACDf.loc[(AFACDf['Date Initiale'] == AFACDf['Date fin / Date prestation']), ['Description']] = AFACDf['Description'] + " " +(AFACDf['Date Initiale'].apply(lambda x: x.strftime('%B %Y')))+ " ("+AFACDf['Type de terme détaillé']+")"


# In[21]:


#Jointure du rapport des CRMPlantID avec le rapport df service charges
OutputDF = pd.merge(AFACDf, SalesforceExtractDF,
              left_on= 'PDLA', right_on= 'dPDL__c', 
              how='inner')


# In[22]:


#On crée les lignes
OutputDF['ID Type Ajustement'] = '6'
OutputDF['Billing Period'] = (pd.to_datetime(DeliveryMonth, format='%Y%m', errors='coerce'))
OutputDF['Billing Period'] = pd.to_datetime(OutputDF['Billing Period']).dt.strftime('%d/%m/%Y')


# In[23]:


#Création d'un index incrémenté pour l'upsert
OutputDF['Id'] = OutputDF.index + 1


# # Gestion d'exceptions

# In[24]:


#Création d'un Dataframe des PDLA communiqué dans l'AFAC_A et qui ne sont pas matché dans Salesforce
SalesforceOutputDF = OutputDF[['PDLA', 'Description', 'Montant HT']]
AFACDf = AFACDf[['PDLA', 'Description', 'Montant HT']]
ExceptionDf = pd.concat([AFACDf, SalesforceOutputDF, SalesforceOutputDF]).drop_duplicates(keep=False)
filename = 'Exception Service_Charges' + DeliveryMonth + '_' + FirstBillingDayOfMonth.strftime('%Y%m%d') + '.xlsx'
ExceptionDf.to_excel(NetworkDir + filename, index=False, encoding='utf-8')


# # Export

# In[25]:


# On renomme les colonnes
OutputDF = OutputDF.rename(columns = ColumnsToRename)


# In[26]:


#On ne garde que les colonnes qui nous intéresse
OutputDF = OutputDF[ColumnOrder]


# In[27]:


#A enlever si upsert
if OutputDF.shape[0] > 0:
    filename = 'Service_Charges' + DeliveryMonth + '_' + FirstBillingDayOfMonth.strftime('%Y%m%d') + '.xlsx'
    OutputDF.to_excel(NetworkDir + filename, index=False, encoding='utf-8')


# # Upsert

# In[28]:


#On ne garde que les informations nécessaires pour uploader la data dans Salesforce, on renomme les colonnes et on créé un dictionnaire pour uplaod

#SalesforceLogResult = NewBEMiscFunctions.SalesforceUpsert(OutputDF, 'Id', 'CRM_Plant_ID__c', 'Type_c', 'Intitule__c', 'Adjustement_Montant__c')
#SalesforceLogResult = pd.DataFrame(SalesforceLogResult)
#SalesforceLogResult.to_excel(ServicechargesUploadLog_Path, index=False)

#S'il y a eu une erreur à l'upload, on lève une erreur. Il faut regarder dans le fichier excel log upload
#if SalesforceLogResult[SalesforceLogResult['success'] == False].shape[0] > 0:
#     ctypes.windll.user32.MessageBoxW(0, "!!! Attention, il y a eu un problème lors de l'upload dans SF du flag de synchronisation entre SAP et Saleforce !!! ", "!!! ERREUR !!! ", 1)





