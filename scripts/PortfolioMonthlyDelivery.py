#!/usr/bin/env python
# coding: utf-8

# In[94]:

import importlib.util
import pandas as pd
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
from openpyxl import load_workbook
import shutil

# def send mail et BEmiskFunctions

Process_Mohamed_Dir = '//fsrsvr06pr/departments/Pricing & Forecasting/General/MO Paris/Process_Mohamed/'
spec_send_email = importlib.util.spec_from_file_location('send_email', Process_Mohamed_Dir + 'Utils/send_email.py')
module_send_email = importlib.util.module_from_spec(spec_send_email)
spec_send_email.loader.exec_module(module_send_email)

spec_NewBEMiscFunctions = importlib.util.spec_from_file_location('NewBEMiscFunctions', Process_Mohamed_Dir + 'Utils/NewBEMiscFunctions.py')
module_NewBEMiscFunctions = importlib.util.module_from_spec(spec_NewBEMiscFunctions)
spec_NewBEMiscFunctions.loader.exec_module(module_NewBEMiscFunctions)


# In[95]:


ExportPath="//fs02mauk/NGSS/French Retail/France Back Office/1. BILLING/Portfolio Monthly/Archive/Portfolio Delivering.xlsx"
Horodatage = datetime.now()
Horodatage = Horodatage.strftime('%Y-%m')
fileName = Horodatage + ".xlsx"


# In[96]:


#On créé un dictionnaire pour renommer les colonnes par rapport aux noms techniques SF
ColumnsToRename = {
    'SiteCRMPlantID__c' : 'CRM Plant ID',
    'Site__r.PCE__c' : 'PCE',
    'Site__r.PDL__c' : 'PDL',
    'dBillingPerimeterName__c' : 'Périmètre de facturation: Perimeter de facturation reference',
    'Site__r.Tech_NomDuCompte__c' : 'Account Name',
    'Site__r.Name' : 'Nom du site',
    'Site__r.PITD__c' : 'PITD',
    'Site__r.Code_PITD__c' : 'Code PITD',
    'Site__r.Compteur_Ville__c' : 'Town',
    'Site__r.StartDate__c' : 'Contract - Start Date',
    'Site__r.EndDate__c' : 'Contract - End Date',
    'dSiteOffer__r.CAR_MWh__c' : 'Contract - CAR',
    'dSiteOffer__r.Profil_consommation__c' : 'Contract - Profil',
    'dReadFrequency__c' : 'Read Frequency',
    'Site__r.TSO__c' : 'TSO',
    'Site__r.DSO_code__c' : 'DSO code',
    'dTariff__c' : 'Contract - Tarif',
    'Site__r.Compte__r.CustomerSegment__c' : 'Segment',
    'Type' : 'Type',
    'Site__r.ZEQ__c' : 'BalancingZone',
    'Site__r.Station_meteo__c'  : 'WeatherStation',
    'dSiteOffer__r.TCC__c' : 'TCC',
    'dSiteOffer__r.AQ__c' : 'AQ',
    'Live' : 'Live'
}


# In[97]:


#On créé une liste pour trier les colonnes dans le bon ordre
ColumnOrder = [
    "CRM Plant ID",
    "PCE",
    "PDL",
    "Périmètre de facturation: Perimeter de facturation reference",
    "Account Name",
    "Nom du site",
    "PITD",
    "Code PITD",
    "Town",
    "Contract - Start Date",
    "Contract - End Date",
    "Contract - CAR",
    "Contract - Profil",
    "Read Frequency",
    "TSO",
    "DSO code",
    "Contract - Tarif",
    "Segment",
    "Type",
    "BalancingZone",
    "WeatherStation",
    "TCC",
    "AQ",
    "Live"
]


# In[98]:


SoqlSBD = "SELECT SiteCRMPlantID__c, Site__r.PCE__c, Site__r.PDL__c, dBillingPerimeterName__c, Site__r.Tech_NomDuCompte__c, "
SoqlSBD += " Site__r.Name, Site__r.PITD__c, Site__r.Code_PITD__c, Site__r.Compteur_Ville__c, Site__r.StartDate__c, Site__r.EndDate__c, "
SoqlSBD += " dSiteOffer__r.CAR_MWh__c, dSiteOffer__r.Profil_consommation__c, dReadFrequency__c, Site__r.TSO__c, Site__r.DSO_code__c, "
SoqlSBD += " dTariff__c, Site__r.Compte__r.CustomerSegment__c, Site__r.ZEQ__c, Site__r.Station_meteo__c, dSiteOffer__r.TCC__c, "
SoqlSBD += " dSiteOffer__r.AQ__c, LiveSBD__c, DataValidFrom__c "
SoqlSBD += " FROM SiteBillingData__c "
SoqlSBD += " Where IsSBDOutputFieldsComputed__c = True and IsObsolete__c = false "


# In[99]:


#detérminer le mois de consommation
today = datetime.today()
first = today.replace(day=1)
lastMonth = first - timedelta(days=1)
print(lastMonth.strftime("%Y%m"))
DeliveryMonth = lastMonth.strftime("%Y%m")

#Constante du mois de facturation
FirstBillingDayOfMonth = (pd.to_datetime(DeliveryMonth, format='%Y%m', errors='coerce')) + relativedelta(months=+1)
FirstConsumptionDayOfMonth = FirstBillingDayOfMonth + relativedelta(days=+1) - relativedelta(months=+1)
LastConsumptionDayOfMonth = FirstBillingDayOfMonth - relativedelta(days=+1)


# In[101]:


# On extrait la data depuis Salesforce
SalesforceExtractDF = module_NewBEMiscFunctions.SalesforceExtract(SoqlSBD)


# In[102]:


SalesforceExtractDF['Site__r.StartDate__c'] = pd.to_datetime(SalesforceExtractDF['Site__r.StartDate__c'])
SalesforceExtractDF['Site__r.EndDate__c'] = pd.to_datetime(SalesforceExtractDF['Site__r.EndDate__c'])


# In[103]:


#On ne garde que les Site Billing Data inférieur à la date de facturation spécifiée
SalesforceExtractDF['DataValidFrom__c'] = pd.to_datetime(SalesforceExtractDF['DataValidFrom__c'])
SalesforceExtractDF = SalesforceExtractDF.loc[(SalesforceExtractDF['DataValidFrom__c'] < FirstBillingDayOfMonth)]


# In[104]:


#On ne garde que le PDL du Site Billing Data le plus récent
SalesforceExtractDF = SalesforceExtractDF.sort_values(by='DataValidFrom__c', ascending=False).drop_duplicates(subset=['SiteCRMPlantID__c'])


# In[105]:


SalesforceExtractDF = SalesforceExtractDF.loc[(SalesforceExtractDF['Site__r.StartDate__c'] <= LastConsumptionDayOfMonth) & (SalesforceExtractDF['Site__r.EndDate__c'] >= FirstConsumptionDayOfMonth)]


# In[106]:


SalesforceExtractDF['Live'] = 'Y'
SalesforceExtractDF['Type'] = ''
SalesforceExtractDF.loc[(SalesforceExtractDF['Site__r.DSO_code__c'] =='GRDF')&((SalesforceExtractDF['dReadFrequency__c'] == '1M') | (SalesforceExtractDF['dReadFrequency__c'] == '6M') | (SalesforceExtractDF['dReadFrequency__c'] == 'MM')), ['Type']] = 'NDM'
SalesforceExtractDF.loc[((SalesforceExtractDF['Site__r.DSO_code__c'] =='GRDF')&(SalesforceExtractDF['dReadFrequency__c'] == 'DD')), ['Type']] = 'DM DSO'
SalesforceExtractDF.loc[(SalesforceExtractDF['Site__r.DSO_code__c'] =='TSO')&(SalesforceExtractDF['dReadFrequency__c'] == 'DD'), ['Type']] = 'DM TSO'
SalesforceExtractDF.loc[(SalesforceExtractDF['Site__r.DSO_code__c'] =='PEG'), ['Type']] = 'PEG'


# In[107]:


# On renomme les colonnes
SalesforceExtractDF = SalesforceExtractDF.rename(columns = ColumnsToRename)


# In[108]:


#On ne garde que les colonnes qui nous intéresse
SalesforceExtractDF = SalesforceExtractDF[ColumnOrder]


# In[109]:


#je supprime la feuille 'Portfolio - CRM' du fichier tmp
book = load_workbook(ExportPath)
std=book['Portfolio - CRM']
book.remove(std)
writer = pd.ExcelWriter(ExportPath, engine = 'openpyxl')
writer.book = book

# il reste une seule feuile, je change le nom de la feuille
name = book.sheetnames
std = book[name[0]]
std.title='Live in '+Horodatage+' CRM'
book.sheetnames

#j'ajoute la nouvelle feuille avec les données exporté
SalesforceExtractDF.to_excel(writer, sheet_name='Portfolio - CRM',index=False, encoding='utf-8')
writer.save()
writer.close()

#je copie le fichier et je renomme
shutil.copy(ExportPath,'//fs02mauk/NGSS/French Retail/France Back Office/1. BILLING/Portfolio Monthly/Portfolio Delivering '+fileName)

#préparation et envoi du mail

from_email = 'rajae.arhoune@gazprom-energy.com'
to_email = ['rajae.arhoune@gazprom-energy.com']
#to_email = ['julien.tamssom@gazprom-energy.com','#GazpromEnergyUKFinancialPlanningandReporting@gazprom-energy.com','francebackoffice@gazprom-energy.com']
subject = 'Portfolio from Salesforce for '+DeliveryMonth+' delivery'
html = 'Hi,<br/><br/>Please find attached the French Portfolio from Salesforce for '+DeliveryMonth +' delivery.'+'<br/><br/><b>Please don\'t forget to refresh all data in the attached file before using it.</b>'
html +='<br/><br/>Best regards<br/>France Back-Office'

attachments='//fs02mauk/NGSS/French Retail/France Back Office/1. BILLING/Portfolio Monthly/Portfolio Delivering '+fileName
module_send_email.send_email(from_email=from_email, to_email=to_email, subject=subject, html=html,attachments=[attachments])