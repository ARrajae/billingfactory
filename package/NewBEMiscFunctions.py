#!/usr/bin/env python
# coding: utf-8

# In[1]:


from simple_salesforce import Salesforce
import collections
import sys
import pandas as pd

LoginSalesforce = 'rajae.arhoune@gazprom-energy.com'
PasswordSalesforce = 'Octobre@2018CLVkbGQdTF2Bp6FeK7jx8GGfI'

def SalesforceExtract(query, typesDict=dict(), typesList=list(), datetimeDict=dict()):

    connection = Salesforce(LoginSalesforce, PasswordSalesforce, '')
    json_result = connection.query_all(query)
    results_columns = query[7: query.lower().find(' from ')].replace(' ', '').split(',')
    
    if not len(json_result['records']):
        return pd.DataFrame(columns=results_columns, dtype=object)
    
    #results = pd.DataFrame.from_records(json_result['records'])
    results = pd.DataFrame(json_result['records'], dtype=object)
    results = results.drop('attributes', axis=1)
    columns = list(results)
    cont = True
    while cont:
        cont = False
        for column in columns:
            if(results.dropna(subset=[column]).shape[0]):
                #if type(results.dropna(subset=[column]).iloc[0][column]) == collections.OrderedDict:
                if not column in results_columns:
                    cont = True
                    results.loc[results[column].isnull(), column] = [collections.OrderedDict([('attributes', pd.NaT)])] * len(results.loc[results[column].isnull(), column])
                    df = pd.DataFrame(list(results[column]), dtype=object)
                    results = results.drop(column, axis=1)
                    df = df.drop('attributes', axis=1)
                    df.columns = column + '.' + df.columns
                    results = pd.concat([results, df], axis=1, sort=True)
                    columns = list(results)
    
    results = results.reindex(columns=results_columns)
    
    if(len(typesDict)): results = results.astype(typesDict)
    if(len(typesList)): results = results.astype(dict(zip(results_columns, typesList)))
    if(len(datetimeDict)):
        for column, datetimeFormat in datetimeDict.items():
            results[column] = pd.to_datetime(results[column], format=datetimeFormat)
    
    return results


# In[2]:


def SalesforceUpsert(ListUpsertDict, ObjectName, FieldForUpsert):
    # Fait un upsert sur la base d'une liste de dictionnaires transmis, pour l'objet indiqué, en utilisant l'identifiant externe indiqué
    # Renvoie un dictionnaire qui est le log d'upload
    
    connection = Salesforce(LoginSalesforce, PasswordSalesforce, '')
    ObjectName = getattr(connection.bulk, ObjectName)
    
    UploadLogMessage = ObjectName.upsert(ListUpsertDict, FieldForUpsert)
       
    return UploadLogMessage


# In[3]:


import win32com.client
import time
from pyxlsb import open_workbook
def RefreshExcelFile(ExcelFilePath):
    # Start an instance of Excel
    xlapp = win32com.client.DispatchEx('Excel.Application')

    # Open the workbook in said instance of Excel
    wb = xlapp.workbooks.open(ExcelFilePath)

    # Optional, e.g. if you want to debug
    # xlapp.Visible = True

    # Refresh all data connections.
    wb.RefreshAll()
    time.sleep(20)
    # Saves the Workbook
    xlapp.DisplayAlerts = False
    wb.Save()
    xlapp.DisplayAlerts = True

    # Closes Excel
    xlapp.Quit()


# In[4]:


def GetMissingBankInSAP(SAPFilePath, BankToCreateInSAP_Path, InputDF):
    wb = open_workbook(SAPFilePath)
    #On créé un df depuis la feuille SAP French Bank Masters. On procède comme cela car le fichier est en .xlsb
    sheet = wb.get_sheet('SAP French Bank Masters')

    SAPBankList = []
    for row in sheet.rows():    
        SAPBankList.append([item.v for item in row])
    SAPBankList = pd.DataFrame(SAPBankList[1:], columns=SAPBankList[0])
    wb.close()
    
    #On ne garde que les codes banques pour pouvoir vérifier si les banques existent dans le référentiel de SAP
    SAPBankList = SAPBankList['BankKeys'].tolist()
    
    #On vérifie si la banque existe dans SAP pour chaque périmètre
    InputDF['IsBankInSAP'] = InputDF['IBAN__c'].apply(lambda iban: (iban[4: 14] in SAPBankList) if iban != None else True)

    # On cherche les IBAN dont la banque n'existe pas dans SAP
    df_PerimeterWithoutBankInSAP = InputDF[InputDF['IsBankInSAP'] == False].reset_index(drop=True)
    df_PerimeterWithoutBankInSAP['BankKeys'] = df_PerimeterWithoutBankInSAP['IBAN__c'].apply(lambda iban: iban[4: 14])
    df_PerimeterWithoutBankInSAP = df_PerimeterWithoutBankInSAP[['IBAN__c', 'BankKeys']]
    
    # Si il y a au moins une banque manquante dans SAP on exporte la liste
    if df_PerimeterWithoutBankInSAP.shape[0] > 0:
        print(f"Please ask David Hartnup to create the following banks in SAP:                \n{df_PerimeterWithoutBankInSAP}")

        df_PerimeterWithoutBankInSAP.to_excel(BankToCreateInSAP_Path, index=False)
    
    return InputDF


# In[5]:


#Délais de paiement par défaut
PaymentTerm_Dict = {
 '1': '0010',
 '6': '0015',
 '11': '0020',
 '16': '0021',
 '21': '0030',
 '31': '0010',
 '36': '0015',
 '41': '0020',
 '46': '0021',
 '51': '0030',
 '61': '0010',
 '66': '0015',
 '71': '0020',
 '76': '0021',
 '81': '0030',
 '86': '0031',
 '96': '0045',
 '111': '0060'
}

import re
def CleanDFForImportInSAP(InputDF):
    # On clean les périmètres dont la banque n'existe pas dans SAP
    InputDF.loc[InputDF['IsBankInSAP'] == False, 'IBAN__c'] = None
    InputDF.loc[InputDF['IsBankInSAP'] == False, 'Type_de_paiement__c'] = 'B'
    
    #On mappe les délais de paiement au format attendu par SAP
    InputDF['Delais_de_paiement__c'] = InputDF['Delais_de_paiement__c'].map(PaymentTerm_Dict)
    
    # On clean le nom du client et numéro de TVA pour import dans SAP
    InputDF['BillingAddressCustomerName__c'] = InputDF['BillingAddressCustomerName__c'].replace(to_replace=r"[-()\"#/@;:<>{}'`+*.=~|.!?,]", value='', regex=True).str[:40]
    InputDF['Numero_de_TVA__c'].replace('NV', '', inplace= True)
    
    return InputDF

