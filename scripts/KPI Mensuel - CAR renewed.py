#!/usr/bin/env python
# coding: utf-8

# In[64]:

import pandas as pd
from openpyxl import load_workbook
import shutil
import importlib.util
from datetime import datetime, timedelta

# def send mail

Process_Mohamed_Dir = '//fsrsvr06pr/departments/Pricing & Forecasting/General/MO Paris/Process_Mohamed/'
spec_send_email = importlib.util.spec_from_file_location('send_email', Process_Mohamed_Dir + 'Utils/send_email.py')
module_send_email = importlib.util.module_from_spec(spec_send_email)
spec_send_email.loader.exec_module(module_send_email)

spec_NewBEMiscFunctions = importlib.util.spec_from_file_location('NewBEMiscFunctions', Process_Mohamed_Dir + 'Utils/NewBEMiscFunctions.py')
module_NewBEMiscFunctions = importlib.util.module_from_spec(spec_NewBEMiscFunctions)
spec_NewBEMiscFunctions.loader.exec_module(module_NewBEMiscFunctions)
# In[65]:


# Initialisation des constantes
ExportPath = '//fs02mauk/NGSS/French Retail/France Back Office/1. BILLING/KPI Monthly/Archive/KPI Mensuel - CAR renewed.xlsx'
#Horodatage
Horodatage = datetime.now()
Horodatage = Horodatage.strftime('%Y-%m')
dateInMail = datetime.now() - timedelta(1)
dateInMail = dateInMail.strftime('%d/%m/%Y')
# In[66]:


# On créé la requête SOQL
SOQLQuery1 = "Select Offre__r.Name, Offre__r.Opportunite__r.CustomerSegment__c, Name, Tarif_acheminement__c, CAR_MWh__c, Profil_consommation__c,AQ__c, Volume_MWh__c, Site__r.EndDate__c, Date_debut__c, Date_fin__c "
SOQLQuery1 += "FROM Detail_offre__c "
SOQLQuery1 += "WHERE Offre_site_retenu__c = TRUE AND Date_debut__c = THIS_MONTH AND Captation_Renouvellement__c ='Renouvellement' AND Tarif_acheminement__c !='PEG'"

# On créé la requête SOQL
SOQLQuery2 = "Select Name, LiveSBD__r.dSiteOffer__r.Offre__r.Opportunite__r.CustomerSegment__c, StartDate__c, EndDate__c, LiveSBD__r.dTariff__c, LiveSBD__r.dCARMWh__c, LiveSBD__r.dProfile__c "
SOQLQuery2 += "FROM SiteNew__c "
SOQLQuery2 += "WHERE EndDate__c = THIS_MONTH AND StatutSite__c !='Annulé' AND LiveSBD__r.dTariff__c !='PEG' AND LiveSBD__r.dIsDeemedSite__c !='Oui'"


# In[67]:


ColumnsToRename1 = {
    
'Offre__r.Name':'Titre de l’offre',
'Offre__r.Opportunite__r.CustomerSegment__c':'Nom du site: Nom du compte: Equipe en charge',
'Name':'Code Offre-Site',
'Tarif_acheminement__c':'Tarif',
'CAR_MWh__c':'CAR - MWh',
'Profil_consommation__c':'Profil',
'AQ__c':'AQ',
'Volume_MWh__c':'Volume - MWh',
'Site__r.EndDate__c':'Nom du site: Date de fin de fourniture - Contrat',
'Date_debut__c':'Date de début',
'Date_fin__c':'Date de fin'
}

ColumnsToRename2 = {
    
'Name': 'Nom du site',
'LiveSBD__r.dSiteOffer__r.Offre__r.Opportunite__r.CustomerSegment__c': 'Equipe en charge',
'StartDate__c': 'Date de début de fourniture - Contrat',
'EndDate__c': 'Date de fin de fourniture - Contrat',
'LiveSBD__r.dTariff__c': 'Tarif Contracté',
'LiveSBD__r.dCARMWh__c': 'CAR Contracté',
'LiveSBD__r.dProfile__c' : 'Profil contracté'

}


# In[68]:


# On extrait la data depuis SF
SalesforceDatadf1 = module_NewBEMiscFunctions.SalesforceExtract(SOQLQuery1)
SalesforceDatadf2 = module_NewBEMiscFunctions.SalesforceExtract(SOQLQuery2)


# In[69]:


# On renomme les colonnes
OutputDF1 = SalesforceDatadf1.rename(columns = ColumnsToRename1)
# On convertit en date les colonnes
OutputDF1['Nom du site: Date de fin de fourniture - Contrat'] = pd.to_datetime(OutputDF1['Nom du site: Date de fin de fourniture - Contrat'])
OutputDF1['Date de début'] = pd.to_datetime(OutputDF1['Date de début'])
OutputDF1['Date de fin'] = pd.to_datetime(OutputDF1['Date de fin'])

# On renomme les colonnes
OutputDF2 = SalesforceDatadf2.rename(columns = ColumnsToRename2)
# On convertit en date les colonnes
OutputDF2['Date de début de fourniture - Contrat'] = pd.to_datetime(OutputDF2['Date de début de fourniture - Contrat'])
OutputDF2['Date de fin de fourniture - Contrat'] = pd.to_datetime(OutputDF2['Date de fin de fourniture - Contrat'])


# In[70]:


book = load_workbook(ExportPath)
std=book['Sites renouvelés last month']
book.remove(std)
std=book['Sites lost last month']
book.remove(std)

writer = pd.ExcelWriter(ExportPath, engine = 'openpyxl')
writer.book = book

OutputDF1.to_excel(writer, sheet_name='Sites renouvelés last month',index=None)
OutputDF2.to_excel(writer, sheet_name='Sites lost last month',index=None)
writer.save()
writer.close()

# In[71]:

fileName = Horodatage+'.xlsx'
shutil.copy('//fs02mauk/NGSS/French Retail/France Back Office/1. BILLING/KPI Monthly/Archive/KPI Mensuel - CAR renewed.xlsx','//fs02mauk/NGSS/French Retail/France Back Office/1. BILLING/KPI Monthly/KPI Mensuel - CAR renewed '+fileName)

#Envoi du mail des KPI Monthly
from_email = 'rajae.arhoune@gazprom-energy.com'
to_email = ['rajae.arhoune@gazprom-energy.com']
#to_email = ['julien.tamssom@gazprom-energy.com','#GazpromEnergyUKFinancialPlanningandReporting@gazprom-energy.com','francebackoffice@gazprom-energy.com']
subject = 'Strategic KPI Data'
html = 'Hi,<br/><br/>Please find attached retention rates for sites to be renewed at '+dateInMail +'<br/><br/><b>Please be informed any deemed site in the past has been removed from the lost data.</b>'
html +='<br/><br/>Best regards<br/>France Back-Office'

attachments='//fs02mauk/NGSS/French Retail/France Back Office/1. BILLING/KPI Monthly/KPI Mensuel - CAR renewed '+fileName
module_send_email.send_email(from_email=from_email, to_email=to_email, subject=subject, html=html,attachments=[attachments])