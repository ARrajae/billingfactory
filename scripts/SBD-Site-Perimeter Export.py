from package import NewBEMiscFunctions
import pandas as pd
from datetime import datetime
import os
import time
import importlib.util

# Initialisation des constantes
CurrentDateTime = datetime.strftime(datetime.now(), '%Y%m%d %H%M')
ExportPath ='//Itss.global/prod/GE/APP/FBE/Data/DataImports/'
horodatage = datetime.now().strftime('%Y%m%d')
CheckPath = '//Itss.global/prod/GE/APP/FBE/Data/DataImports/Archive/'+horodatage+'/'

# def send mail

Process_Mohamed_Dir = '//fsrsvr06pr/departments/Pricing & Forecasting/General/MO Paris/Process_Mohamed/'
spec_send_email = importlib.util.spec_from_file_location('send_email', Process_Mohamed_Dir + 'Utils/send_email.py')
module_send_email = importlib.util.module_from_spec(spec_send_email)
spec_send_email.loader.exec_module(module_send_email)


#SBD
SOQLQuery1 = "SELECT Name, SiteCRMPlantID__c, CreatedDate__c, DataValidFrom__c, DataValidUntil__c, dBillingPerimeterName__c, dPCE__c, dPDL__c, dFirmAnnualCapacityMWhDay__c, dCARMWh__c, dProfile__c, dTariff__c, StorageCostEur__c, dIsStorageMonthly__c, CTAStandardRateEurMonth__c, dGasPriceEurMWh__c, dPriceType__c, CustomerPriceDataSource__c, dIndex1Name__c, dBasketReference__c, dReadFrequency__c, FixTermEurMonth__c, ProportionnalTermEurMWh__c, CSPGStandardRateEurMWh__c, dCSPGExemptionPct__c, dCTAExemptionPct__c, CTSSGStandardRateEurMWh__c, dCTSSExemptionPct__c, TICGNStandardRateEurMWh__c, dTICGNExemptionPct__c, DSOCostEurMonth__c, TSOCostEurMonth__c, CTADSOStandardRateEurMonth__c, CTATSOStandardRateEurMonth__c, dTICGNLowerRate1Pct__c,  dTICGNLowerRate2Pct__c, dTICGNLowerRate1EurMwh__c, dTICGNLowerRate2EurMwh__c, CEEStandardRateEurMWh__c, dIsAbonnementMinore__c, dCoefficientGasVolumeToEnergy__c, IsObsolete__c"
SOQLQuery1 += " From SiteBillingData__c"
SOQLQuery1 += " Where LastModifiedDate >= LAST_N_DAYS:2"

#Site
SOQLQuery2 = "SELECT CRM_PlantID__c, Compte__r.Name, StartDate__c, EndDate__c, Name, PCE__c, PDL__c, TSO__c, DSO_code__c, ZEQ__c, Code_PITD__c, Compteur_Code_Postal__r.CodeStationMeteo__c, NTR__c"
SOQLQuery2 += " FROM SiteNew__c"
SOQLQuery2 += " WHERE LastModifiedDate >= LAST_N_DAYS:2"

#Perimeter
SOQLQuery3 = "SELECT Numero_de_contrat__c, IsVATExemption__c, IsDMBilledonAFAC__c, InvoiceManualCheck__c, BillingFrequency__c, BillingFrequencyPattern__c, OnDemandDateInvoice__c, BillingLastConsumptionDayOfMonth__c, BillingGenerationDayOfMonth__c, BillingAddressCustomerName__c, Adresse_de_facturation_Code_Postal__c, Adresse_de_facturation_Ville__c, Numero_de_TVA__c, Compte_SAP__c, Type_de_paiement__c, Delais_de_paiement__c, IsBillingBlocked__c, BillingCompany__c"
SOQLQuery3 += " FROM Contrat_BDB__c"
SOQLQuery3 += " where LastModifiedDate >= LAST_N_DAYS:2"

#########################################################################       préparation du fichier import des Site
# On créé un dictionnaire avec les valeurs par défaut pour les champs qu'on ne requête pas mais qui doivent être importés dans le BE
DictDefaultValuesSite = {
    'Perimeter Start Date' :  0,
    'Live SBD: dBilling Perimeter: Perimeter de facturation reference' : 30000000,
    'BDB_PlantID' :  '',
    'Live SBD: dTariff' :  '',
    'Live SBD: dRead Frequency' :  '',
    'Thermal Coefficient' :  '',
    'Live SBD: dProfile' :  '',
    'CustomerPriceDataSource__' :  '',
    'Live SBD: dPrice Type' :  '',
    'Live SBD: dIndex Name' :  '',
    'Panier Ref' :  '',
    'Live SBD: dGas Price - EUR/MWh' :  '',
    'Live SBD: Fix Term - EUR/Month' :  '',
    'Live SBD: Proportionnal Term - EUR/MWh' :  '',
    'Live SBD: CTA - Standard Rate - EUR/Month' :  '',
    'Live SBD: Storage Cost - EUR' :  '',
    'Live SBD: dIsStorageMonthlyBool' :  '',
    'Live SBD: dIsAbonnementMinoreBool' :  '',
    'DSO Status' :  '',
    'Live SBD: dCTA Exemption - %' :  '',
    'Live SBD: dTICGN - Exemption - %' :  '',
    'Live SBD: dTICGN - Lower Rate 1 - EUR/MWh' :  '',
    'Live SBD: dTICGN - Lower Rate 2 - EUR/MWh' :  '',
    'Live SBD: CTSSG - Standard Rate - EUR/MWh' :  '',
    'Live SBD: CSPG - Standard Rate - EUR/MWh' :  '',
    'Live SBD: dCSPG - Exemption - %' :  '',
    'Live SBD: dTICGN - Lower Rate 1 - %' :  '',
    'Tech_CompteurCodePostal__c' :  '',
    'Town' :  '',
    'Live SBD: Reference Site Billing Data' :  '',
    'Live SBD: Données Facturation du site' :  '',
    'Live SBD: TICGN - Standard Rate - EUR/MWh' :  '',
    'Live SBD: dTICGN - Lower Rate 2 - %' :  '',
    'Live SBD: dCTSSG - Exemption - %' :  '',
    'Address' :  '',
    'Live SBD: dSite Account: Group Account: Account Name' :  '',
    'Rue - Ligne 2' :  '',
    'PITD' :  '',
    'Zip Code: Post Code' :  '',
    'Zip Code: INSEE Code' :  '',
    'Nom du DSO' :  '',
    'Live SBD: dFirm Annual Capacity - MWh/Day' :  '',
    'CEE Scope' :  '',
    'Live SBD: dCAR - MWh' : '',
    'Zip Code: Gas Type' : 'H',
    'Zip Code: Post Code' : '',
    'Zip Code: INSEE Code' : ''
}

#On créé un dictionnaire pour renommer les colonnes par rapport aux noms techniques SF
ColumnsToRenameSite = {
    'CRM_PlantID__c' : 'CRM Plant ID',
    'Compte__r.Name' : 'Live SBD: dSite Account: Account Name',
    'Name' : 'Nom du site',
    'StartDate__c' : 'StartDate',
    'EndDate__c' : 'EndDate',
    'PCE__c' : 'Live SBD: dPCE',
    'PDL__c' : 'Live SBD: dPDL',
    'TSO__c' : 'TSO',
    'DSO_code__c' : 'DSO code',
    'ZEQ__c' : 'ZEQ',
    'Code_PITD__c' : 'Code PITD',
    'Compteur_Code_Postal__r.CodeStationMeteo__c' : 'Zip Code: Weather Station Code number',
    'NTR__c' : 'NTR'
}
#On créé une liste pour trier les colonnes dans le bon ordre
ColumnOrderSite = [
 'Perimeter Start Date',
 'CRM Plant ID',
 'Live SBD: dSite Account: Group Account: Account Name',
 'Live SBD: dSite Account: Account Name',
 'Live SBD: dBilling Perimeter: Perimeter de facturation reference',
 'BDB_PlantID',
 'Live SBD: Reference Site Billing Data',
 'Nom du site',
 'Address',
 'Rue - Ligne 2',
 'Zip Code: Post Code',
 'Town',
 'StartDate',
 'EndDate',
 'Live SBD: dPCE',
 'Live SBD: dPDL',
 'TSO',
 'DSO code',
 'Nom du DSO',
 'ZEQ',
 'Zip Code: Gas Type',
 'Code PITD',
 'PITD',
 'Live SBD: dTariff',
 'Zip Code: Weather Station Code number',
 'Live SBD: dRead Frequency',
 'DSO Status',
 'NTR',
 'Live SBD: dCAR - MWh',
 'Thermal Coefficient',
 'Live SBD: dProfile',
 'Live SBD: dFirm Annual Capacity - MWh/Day',
 'Live SBD: Données Facturation du site',
 'Live SBD: dPrice Type',
 'Live SBD: dIndex Name',
 'Panier Ref',
 'Live SBD: dGas Price - EUR/MWh',
 'Live SBD: Fix Term - EUR/Month',
 'Live SBD: Proportionnal Term - EUR/MWh',
 'Live SBD: CTA - Standard Rate - EUR/Month',
 'Live SBD: Storage Cost - EUR',
 'Live SBD: dIsStorageMonthlyBool',
 'Live SBD: dIsAbonnementMinoreBool',
 'Live SBD: dCTA Exemption - %',
 'Live SBD: TICGN - Standard Rate - EUR/MWh',
 'Live SBD: dTICGN - Exemption - %',
 'Live SBD: dTICGN - Lower Rate 1 - EUR/MWh',
 'Live SBD: dTICGN - Lower Rate 1 - %',
 'Live SBD: dTICGN - Lower Rate 2 - EUR/MWh',
 'Live SBD: dTICGN - Lower Rate 2 - %',
 'Live SBD: CTSSG - Standard Rate - EUR/MWh',
 'Live SBD: dCTSSG - Exemption - %',
 'Live SBD: CSPG - Standard Rate - EUR/MWh',
 'Live SBD: dCSPG - Exemption - %',
 'Zip Code: INSEE Code',
 'CEE Scope'
]
# On extrait la data depuis SF
SalesforceDatadf = NewBEMiscFunctions.SalesforceExtract(SOQLQuery2)
# On rajouter les colonnes avec valeurs par défaut
OutputSite = SalesforceDatadf.assign(**DictDefaultValuesSite)
# On renomme les colonnes
OutputSite = OutputSite.rename(columns = ColumnsToRenameSite)
# On convertit en date les colonnes
OutputSite['StartDate'] = pd.to_datetime(OutputSite['StartDate']).dt.strftime("%d/%m/%Y")
OutputSite['EndDate'] = pd.to_datetime(OutputSite['EndDate']).dt.strftime("%d/%m/%Y")
# On trie les colonnes
OutputSite = OutputSite[ColumnOrderSite]
SiteFileName = 'SF-Sites ' + CurrentDateTime + '.xlsx'
#On exporte le fichier des SBD directement dans le DataImport
OutputSite.to_excel(ExportPath + SiteFileName , index=False)

#########################################################################       préparation du fichier import des SBD
#On créé un dictionnaire pour renommer les colonnes par rapport aux noms techniques SF
ColumnsToRenameSBD = {
    'Name' : 'Reference Site Billing Data',
    'SiteCRMPlantID__c' : 'CRM Plant ID',
    'CreatedDate__c' : 'Created Date',
    'DataValidFrom__c' : 'Data Valid From',
    'DataValidUntil__c' : 'Data Valid Until',
    'dBillingPerimeterName__c' : 'Perimeter Reference',
    'dPCE__c' : 'PCE',
    'dPDL__c' : 'PDL',
    'dFirmAnnualCapacityMWhDay__c' : 'Firm Annual Capacity',
    'dCARMWh__c' : 'CAR - MWh',
    'dProfile__c' : 'Profil',
    'dTariff__c' : 'Tariff',
    'StorageCostEur__c' : 'Storage Cost - EUR',
    'dIsStorageMonthly__c' : 'Is Storage Monthly ?',
    'CTAStandardRateEurMonth__c' : 'CTA - Standard Rate - EUR/Month',
    'dGasPriceEurMWh__c' : 'Gas Price - EUR/MWh',
    'dPriceType__c' : 'Price Type',
    'CustomerPriceDataSource__c' : 'Price Source',
    'dIndex1Name__c' : 'Index Name',
    'dBasketReference__c' : 'Panier Reference',
    'dReadFrequency__c' : 'Read Frequency',
    'FixTermEurMonth__c' : 'Fix Term - EUR/Month',
    'ProportionnalTermEurMWh__c' : 'Proportionnal Term - EUR/MWh',
    'CSPGStandardRateEurMWh__c' : 'CSPG - Standard Rate - EUR/MWh',
    'dCSPGExemptionPct__c' : 'CSPG - Exemption - %',
    'dCTAExemptionPct__c' : 'CTA - Exemption - %',
    'CTSSGStandardRateEurMWh__c' : 'CTSSG - Standard Rate - EUR/MWh',
    'dCTSSExemptionPct__c' : 'CTSSG - Exemption - %',
    'TICGNStandardRateEurMWh__c' : 'TICGN - Standard Rate - EUR/MWh',
    'dTICGNExemptionPct__c' : 'TICGN - Exemption - %',
    'DSOCostEurMonth__c' : 'DSO Cost - EUR/Month',
    'TSOCostEurMonth__c' : 'TSO Cost - EUR/Month',
    'CTADSOStandardRateEurMonth__c' : 'CTA DSO - Standard Rate - EUR/Month',
    'CTATSOStandardRateEurMonth__c' : 'CTA TSO - Standard Rate - EUR/Month',
    'dTICGNLowerRate1Pct__c' : 'TICGN - Lower Rate 1 - %',
    'dTICGNLowerRate2Pct__c' : 'TICGN - Lower Rate 2 - %',
    'dTICGNLowerRate1EurMwh__c' : 'TICGN - Lower Rate 1 - EUR/MWh',
    'dTICGNLowerRate2EurMwh__c' : 'TICGN - Lower Rate 2 - EUR/MWh',
    'CEEStandardRateEurMWh__c' : 'CEE - Standard Rate - EUR/MWh',
    'dIsAbonnementMinore__c' : 'Is abonnement minore ?',
    'dCoefficientGasVolumeToEnergy__c' : 'Thermal Coefficient',
    'IsObsolete__c' : 'Is Obsolete ?'
}
#On créé une liste pour trier les colonnes dans le bon ordre
ColumnOrderSBD = [
    'Reference Site Billing Data',
    'CRM Plant ID',
    'Created Date',
    'Data Valid From',
    'Data Valid Until',
    'Perimeter Reference',
    'PCE',
    'PDL',
    'Firm Annual Capacity',
    'CAR - MWh',
    'Profil',
    'Tariff',
    'Storage Cost - EUR',
    'Is Storage Monthly ?',
    'CTA - Standard Rate - EUR/Month',
    'Gas Price - EUR/MWh',
    'Price Type',
    'Price Source',
    'Index Name',
    'Panier Reference',
    'Read Frequency',
    'Fix Term - EUR/Month',
    'Proportionnal Term - EUR/MWh',
    'CSPG - Standard Rate - EUR/MWh',
    'CSPG - Exemption - %',
    'CTA - Exemption - %',
    'CTSSG - Standard Rate - EUR/MWh',
    'CTSSG - Exemption - %',
    'TICGN - Standard Rate - EUR/MWh',
    'TICGN - Exemption - %',
    'DSO Cost - EUR/Month',
    'TSO Cost - EUR/Month',
    'CTA DSO - Standard Rate - EUR/Month',
    'CTA TSO - Standard Rate - EUR/Month',
    'TICGN - Lower Rate 1 - %',
    'TICGN - Lower Rate 2 - %',
    'TICGN - Lower Rate 1 - EUR/MWh',
    'TICGN - Lower Rate 2 - EUR/MWh',
    'CEE - Standard Rate - EUR/MWh',
    'Is abonnement minore ?',
    'Thermal Coefficient',
    'Is Obsolete ?'
]
# On extrait la data depuis SF
SalesforceDatadf = NewBEMiscFunctions.SalesforceExtract(SOQLQuery1)
#On convertit les True/False en O/1 et les datetime au bon format
SalesforceDatadf.loc[SalesforceDatadf['dIsAbonnementMinore__c'] == 'Oui', ['dIsAbonnementMinore__c']] = 1
SalesforceDatadf.loc[SalesforceDatadf['dIsAbonnementMinore__c'] == 'Non', ['dIsAbonnementMinore__c']] = 0
SalesforceDatadf['IsObsolete__c'] = SalesforceDatadf['IsObsolete__c'] * 1
SalesforceDatadf['CreatedDate__c'] = pd.to_datetime(SalesforceDatadf['CreatedDate__c']).dt.strftime('%d/%m/%Y')
SalesforceDatadf['DataValidFrom__c'] = pd.to_datetime(SalesforceDatadf['DataValidFrom__c']).dt.strftime('%d/%m/%Y')
# On renomme les colonnes
OutputSBD = SalesforceDatadf.rename(columns = ColumnsToRenameSBD)
#print(OutputDF.IsSBDOutputFieldsComputed__c)
# On trie les colonnes
OutputSBD = OutputSBD[ColumnOrderSBD]
SBDfileName = 'Site Billing Data Object ' + CurrentDateTime + '.xlsx'
OutputSBD.to_excel(ExportPath + SBDfileName, index=False)

#########################################################################       préparation du fichier import des perimetres

PaymentTermToString = {
                  '1'  : 'EOM0',
                  '11' : 'EOM0',
                  '111': 'EOM1',
                  '16' : 'EOM0',
                  '21' : 'EOM0',
                  '31' : 'EOM0',
                  '36' : 'EOM0',
                  '41' : 'EOM0',
                  '46' : 'EOM0',
                  '51' : 'EOM0',
                  '6'  : 'EOM0',
                  '61' : 'EOM0',
                  '66' : 'EOM0',
                  '71' : '20M0',
                  '76' : 'EOM0',
                  '81' : 'EOM0',
                  '86' : 'EOM0',
                  '96' : '15M1'
                      }
# On créé un dictionnaire avec les valeurs par défaut pour les champs qu'on ne requête pas mais qui doivent être importés dans le BE
DictDefaultValuesPerim = {
    'Billing address - Line 1' : '',
    'Billing address - Line 2' : '',
    'Billing address - Country': '',
    'Contact Name' : '',
    'IBAN' : '',
    'SWIFT' : '',
    'Code Banque' : '',
    'Code Guichet' : '',
    'Numéro de compte bancaire' : '',
    'Clé RIB' : '',
    'Client reference' : '',
    'Cover Letter Duplicata' : '',
    'CLDupl - Address Line 1' : '',
    'CLDupl - Address Line 2' : '',
    'CLDupl - Country' : '',
    'CLDupl - Name of the company' : '',
    'CLDupl - Town' : '',
    'CLDupl - Zip Code' : '',
    'Bank Name' : '',
    'Bank Address' : '',
    'Forced Show'  : 'TRUE'

}
#On créé un dictionnaire pour renommer les colonnes par rapport aux noms techniques SF
ColumnsToRenamePerim = {
    'Numero_de_contrat__c' : 'Perimeter Reference',
    'IsVATExemption__c' : 'Is VAT Exempt',
    'IsDMBilledonAFAC__c' : 'Actual Reads',
    'InvoiceManualCheck__c' : 'Manual Invoice Check',
    'BillingFrequency__c' : 'Billing Frequency',
    'BillingFrequencyPattern__c' : 'Billing Frequency Pattern',
    'OnDemandDateInvoice__c' : 'Billing On Demand Date',
    'BillingLastConsumptionDayOfMonth__c' : 'Billing Day of Month',
    'BillingGenerationDayOfMonth__c' : 'Billing Generation Day of Month',
    'BillingAddressCustomerName__c' : 'Billing address - Name of the company',
    'Adresse_de_facturation_Code_Postal__c' : 'Billing address - Zip Code',
    'Adresse_de_facturation_Ville__c' : 'Billing address - Town',
    'Numero_de_TVA__c' : 'VAT Number',
    'Compte_SAP__c' : 'SAP Account',
    'Type_de_paiement__c' : 'Payment Type',
    'Payment' : 'SF Payment Term',
    'BillingCompany__c' : 'Company Code'
}
#On créé une liste pour trier les colonnes dans le bon ordre
ColumnOrderPerim = [
    'Perimeter Reference',
    'Is VAT Exempt',
    'Actual Reads',
    'Manual Invoice Check',
    'Billing Frequency',
    'Billing Frequency Pattern',
    'Billing On Demand Date',
    'Billing Day of Month',
    'Billing Generation Day of Month',
    'Billing address - Name of the company',
    'Billing address - Line 1',
    'Billing address - Line 2',
    'Billing address - Zip Code',
    'Billing address - Town',
    'Billing address - Country',
    'Contact Name',
    'VAT Number',
    'SAP Account',
    'Payment Type',
    'SF Payment Term',
    'IBAN',
    'SWIFT',
    'Code Banque',
    'Code Guichet',
    'Numéro de compte bancaire',
    'Clé RIB',
    'Client reference',
    'Cover Letter Duplicata',
    'CLDupl - Address Line 1',
    'CLDupl - Address Line 2',
    'CLDupl - Country',
    'CLDupl - Name of the company',
    'CLDupl - Town',
    'CLDupl - Zip Code',
    'Bank Name',
    'Bank Address',
    'Forced Show',
    'Company Code'
]
# On extrait la data depuis SF
SalesforceDatadf = NewBEMiscFunctions.SalesforceExtract(SOQLQuery3)
#On convertit les True/False en O/1
SalesforceDatadf['IsVATExemption__c'] = SalesforceDatadf['IsVATExemption__c'] * 1
SalesforceDatadf['IsDMBilledonAFAC__c'] = SalesforceDatadf['IsDMBilledonAFAC__c'] * 1
SalesforceDatadf['InvoiceManualCheck__c'] = SalesforceDatadf['InvoiceManualCheck__c'] * 1
SalesforceDatadf['BillingFrequency__c'] = SalesforceDatadf['BillingFrequency__c'] + " Monthly"
SalesforceDatadf['Payment'] = SalesforceDatadf['Delais_de_paiement__c'].map(PaymentTermToString)
SalesforceDatadf['BillingCompany__c'] = 1300


# On retire le compte SAP des périmètres identifiés comme étant à ne pas facturer
def GetSAPAccountNumber(Row):
    if Row['IsBillingBlocked__c'] == True:
        return None
    else:
        return Row['Compte_SAP__c']


SalesforceDatadf['Compte_SAP__c'] = SalesforceDatadf.apply(GetSAPAccountNumber, axis=1)

# On rajouter les colonnes avec valeurs par défaut
OutputPerim = SalesforceDatadf.assign(**DictDefaultValuesPerim)
# On renomme les colonnes
OutputPerim = OutputPerim.rename(columns = ColumnsToRenamePerim)
# On trie les colonnes
OutputPerim = OutputPerim[ColumnOrderPerim]
PeimFileName = 'SF-Perimeters NewBE ' + CurrentDateTime + '.xlsx'
OutputPerim.to_excel(ExportPath + PeimFileName, index=False)



# check import dans le BE
time.sleep(240)
Ckecklist = os.listdir(CheckPath)
if SiteFileName not in Ckecklist:
    from_email = 'rajae.arhoune@gazprom-energy.com'
    to_email = ['rajae.arhoune@gazprom-energy.com']
    subject = html = 'Import ' + SiteFileName + 'KO'
    module_send_email.send_email(from_email=from_email, to_email=to_email, subject=subject, html=html)

if SBDfileName not in Ckecklist:
    from_email = 'rajae.arhoune@gazprom-energy.com'
    to_email = ['rajae.arhoune@gazprom-energy.com']
    subject = html = 'Import ' + SBDfileName + 'KO'
    module_send_email.send_email(from_email=from_email, to_email=to_email, subject=subject, html=html)

if PeimFileName not in Ckecklist:
    from_email = 'rajae.arhoune@gazprom-energy.com'
    to_email = ['rajae.arhoune@gazprom-energy.com']
    subject = html = 'Import ' + PeimFileName + 'KO'
    module_send_email.send_email(from_email=from_email, to_email=to_email, subject=subject, html=html)